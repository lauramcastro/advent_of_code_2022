defmodule AdventOfCode2022.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      version: "0.1.0",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      # Docs
      name: "My take on Advent of Code 2022",
      source_url: "https://gitlab.com/lauramcastro/advent_of_code_2022",
      homepage_url: "http://gitlab.com/lauramcastro/advent_of_code_2022",
      docs: [
        extras: ["README.md"]
      ],
      # Coverage
      test_coverage: [tool: ExCoveralls],
      preferred_cli_env: [
        coveralls: :test
      ]
    ]
  end

  # Dependencies listed here are available only for this
  # project and cannot be accessed from applications inside
  # the apps folder.
  #
  # Run "mix help deps" for examples and options.
  defp deps do
    [
      {:credo, "~> 1.6.7", only: [:dev, :test], runtime: false},
      {:ex_doc, "~> 0.29.1", only: :dev, runtime: false},
      {:excoveralls, "~> 0.15.1", only: :test}
    ]
  end
end
