defmodule Advent10 do
  @moduledoc """
  Documentation for `Advent10`.
  """

  @doc """
  Calculates the signal strength at certain points following a given program.

  ## Examples

      iex> Advent10.signal_strength([["noop"],["addx", 3],["addx", -5]])
      -720

  """
  @spec signal_strength(list(list(String.t() | integer()))) :: integer()
  def signal_strength(program) do
    interesting_points = [{20, 1}, {60, 1}, {100, 1}, {140, 1}, {180, 1}, {220, 1}]

    do_signal_strength(0, program, interesting_points)
    |> Enum.map(fn {p, v} -> p * v end)
    |> Enum.sum()
  end

  defp do_signal_strength(_, [], interesting_points), do: interesting_points

  defp do_signal_strength(cycle, [["noop"] | rest], interesting_points),
    do: do_signal_strength(cycle + 1, rest, interesting_points)

  defp do_signal_strength(cycle, [["addx", value] | rest], interesting_points) do
    new_ipoints =
      Enum.map(interesting_points, fn
        {ncycle, nvalue} when ncycle > cycle + 2 -> {ncycle, nvalue + value}
        other -> other
      end)

    do_signal_strength(cycle + 2, rest, new_ipoints)
  end

  @doc """
  Calculates the crt output following a given program.

  """
  @spec crt_output(list(list(String.t() | integer()))) :: String.t()
  def crt_output(program) do
    do_crt_output(1, 1, program, []) |> Enum.join()
  end

  defp do_crt_output(_cycle, _x, [], crt), do: Enum.reverse(crt)

  defp do_crt_output(cycle, x, [["noop"] | rest], crt) do
    # each cycle adds something to crt
    # adds lit pixel ("#") if length(crt) [modulo the size of the crt screen, 40] is in the range X-1..X+1
    # adds dark pixel (".") otherwise
    do_crt_output(cycle + 1, x, rest, [
      if(rem(length(crt), 40) in (x - 1)..(x + 1), do: "#", else: ".") | crt
    ])
  end

  defp do_crt_output(cycle, x, [["addx", value] | rest], crt) do
    do_crt_output(cycle + 2, x + value, rest, [
      if(rem(length(crt) + 1, 40) in (x - 1)..(x + 1), do: "#", else: "."),
      if(rem(length(crt), 40) in (x - 1)..(x + 1), do: "#", else: ".") | crt
    ])
  end
end
