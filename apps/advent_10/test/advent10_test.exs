defmodule Advent10Test do
  use ExUnit.Case
  doctest Advent10

  test "answer to sample" do
    program =
      Utils.get_input("priv/sample_input", fn x ->
        case String.split(x) do
          [op, amount] -> [op, String.to_integer(amount)]
          op -> op
        end
      end)

    assert Advent10.signal_strength(program) == 13_140
  end

  test "answer to first challenge" do
    program =
      Utils.get_input("priv/input", fn x ->
        case String.split(x) do
          [op, amount] -> [op, String.to_integer(amount)]
          op -> op
        end
      end)

    assert Advent10.signal_strength(program) == 16_060
  end

  test "answer to sample (second part)" do
    program =
      Utils.get_input("priv/sample_input", fn x ->
        case String.split(x) do
          [op, amount] -> [op, String.to_integer(amount)]
          op -> op
        end
      end)

    assert Advent10.crt_output(program) ==
             "##..##..##..##..##..##..##..##..##..##.." <>
               "###...###...###...###...###...###...###." <>
               "####....####....####....####....####...." <>
               "#####.....#####.....#####.....#####....." <>
               "######......######......######......####" <>
               "#######.......#######.......#######....."
  end

  test "answer to second challenge" do
    program =
      Utils.get_input("priv/input", fn x ->
        case String.split(x) do
          [op, amount] -> [op, String.to_integer(amount)]
          op -> op
        end
      end)

    assert Advent10.crt_output(program)
           |> String.graphemes()
           |> Enum.chunk_every(40)
           |> Enum.map(fn x -> Enum.join(x) end) == [
             "###...##...##..####.#..#.#....#..#.####.",
             "#..#.#..#.#..#.#....#.#..#....#..#.#....",
             "###..#..#.#....###..##...#....####.###..",
             "#..#.####.#....#....#.#..#....#..#.#....",
             "#..#.#..#.#..#.#....#.#..#....#..#.#....",
             "###..#..#..##..####.#..#.####.#..#.#...."
           ]
  end
end
