defmodule Advent07Test do
  use ExUnit.Case
  doctest Advent07

  test "answer to first challenge" do
    filesystem_commands = Utils.get_input("priv/input", fn x -> String.split(x) end)

    filesystem = Advent07.filesystem_from_commands(filesystem_commands)

    assert Advent07.total_within_size(filesystem, 100_000) == 1_770_595
  end

  test "answer to second challenge" do
    filesystem_commands = Utils.get_input("priv/input", fn x -> String.split(x) end)

    filesystem = Advent07.filesystem_from_commands(filesystem_commands)

    assert Advent07.best_choice_for_deletion(filesystem, 70_000_000, 30_000_000) == 2_195_372
  end
end
