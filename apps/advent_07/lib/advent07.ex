defmodule Advent07 do
  @moduledoc """
  Documentation for `Advent07`.
  """

  require NaryTree

  @doc """
  Creates an N-Ary tree representing a filesystem from commands that explore said filesystem.

  ## Examples

      > Advent07.filesystem_from_commands([["$", "cd", "/"], ["$", "ls"], ["dir", "a"], ["14848514", "b.txt"], ["8504156", "c.dat"], ["dir", "d"],
      ...>                                    ["$", "cd", "a"], ["$", "ls"], ["dir", "e"], ["29116", "f"], ["2557", "g"], ["62596", "h.lst"],
      ...>                                    ["$", "cd", "e"], ["$", "ls"], ["584", "i"],
      ...>                                    ["$", "cd", ".."], ["$", "cd", ".."],
      ...>                                    ["$", "cd", "d"], ["$", "ls"], ["4060174", "j"], ["8033020", "d.log"], ["5626152", "d.ext"], ["7214296", "k"],
      ...>                                   ]) |> NaryTree.print_tree()
      :ok

  """
  @spec filesystem_from_commands(list(String.t())) :: NaryTree.t()
  def filesystem_from_commands([["$", "cd", "/"] | commands]) do
    root = NaryTree.Node.new("/") |> NaryTree.new()
    do_filesystem_from_commands(commands, root, NaryTree.root(root).id)
  end

  defp do_filesystem_from_commands([], filesystem, _id), do: filesystem

  defp do_filesystem_from_commands([["$", "ls"] | commands], filesystem, parent_id) do
    {rest_commands, contents} = do_subdirectory_contents_from_commands(commands, [])

    filesystem =
      Enum.reduce(contents, filesystem, fn c, acc -> NaryTree.add_child(acc, c, parent_id) end)

    do_filesystem_from_commands(rest_commands, filesystem, parent_id)
  end

  defp do_filesystem_from_commands([["$", "cd", ".."] | commands], filesystem, parent_id) do
    {:ok, parent} = NaryTree.fetch(filesystem, parent_id)
    ancestor = NaryTree.parent(parent, filesystem)

    do_filesystem_from_commands(
      commands,
      filesystem,
      ancestor.id
    )
  end

  defp do_filesystem_from_commands([["$", "cd", name] | commands], filesystem, parent_id) do
    {:ok, parent} = NaryTree.fetch(filesystem, parent_id)
    subdir = NaryTree.children(parent, filesystem) |> Enum.find(fn n -> n.name == name end)
    do_filesystem_from_commands(commands, filesystem, subdir.id)
  end

  defp do_subdirectory_contents_from_commands([], children) do
    {[], Enum.reverse(children)}
  end

  defp do_subdirectory_contents_from_commands(rest_commands = [["$" | _rest] | _more], children) do
    {rest_commands, Enum.reverse(children)}
  end

  defp do_subdirectory_contents_from_commands([["dir", name] | rest], acc) do
    do_subdirectory_contents_from_commands(rest, [NaryTree.Node.new(name) | acc])
  end

  defp do_subdirectory_contents_from_commands([[size, name] | rest], acc) do
    do_subdirectory_contents_from_commands(rest, [
      NaryTree.Node.new(name, String.to_integer(size)) | acc
    ])
  end

  @doc """
  Finds and sums all subdirectories which contain less than a given size.

  ## Examples

      iex> Advent07.filesystem_from_commands([["$", "cd", "/"], ["$", "ls"], ["dir", "a"], ["14848514", "b.txt"], ["8504156", "c.dat"], ["dir", "d"],
      ...>                                    ["$", "cd", "a"], ["$", "ls"], ["dir", "e"], ["29116", "f"], ["2557", "g"], ["62596", "h.lst"],
      ...>                                    ["$", "cd", "e"], ["$", "ls"], ["584", "i"],
      ...>                                    ["$", "cd", ".."], ["$", "cd", ".."],
      ...>                                    ["$", "cd", "d"], ["$", "ls"], ["4060174", "j"], ["8033020", "d.log"], ["5626152", "d.ext"], ["7214296", "k"],
      ...>                                   ]) |> Advent07.total_within_size(100_000)
      95437

  """
  @spec total_within_size(NaryTree.t(), non_neg_integer()) :: non_neg_integer()
  def total_within_size(filesystem, limit) do
    node = NaryTree.root(filesystem)

    do_get_directories([node], filesystem, [])
    |> Enum.map(fn n -> size(n, filesystem) end)
    |> Enum.filter(fn s -> s <= limit end)
    |> Enum.sum()
  end

  defp do_get_directories([], _filesystem, acc), do: acc

  defp do_get_directories([node = %NaryTree.Node{} | rest], filesystem, acc) do
    subdirs =
      NaryTree.children(node, filesystem) |> Enum.filter(fn n -> not NaryTree.has_content?(n) end)

    do_get_directories(
      rest ++ subdirs,
      filesystem,
      [node | acc]
    )
  end

  defp size(node, filesystem) do
    if NaryTree.is_leaf?(node) do
      node.content
    else
      NaryTree.children(node, filesystem)
      |> Enum.reduce(0, fn x, acc -> size(x, filesystem) + acc end)
    end
  end

  @doc """
  Finds the best choice for deletion to have enough space on a given disk.

  ## Examples

      iex> Advent07.filesystem_from_commands([["$", "cd", "/"], ["$", "ls"], ["dir", "a"], ["14848514", "b.txt"], ["8504156", "c.dat"], ["dir", "d"],
      ...>                                    ["$", "cd", "a"], ["$", "ls"], ["dir", "e"], ["29116", "f"], ["2557", "g"], ["62596", "h.lst"],
      ...>                                    ["$", "cd", "e"], ["$", "ls"], ["584", "i"],
      ...>                                    ["$", "cd", ".."], ["$", "cd", ".."],
      ...>                                    ["$", "cd", "d"], ["$", "ls"], ["4060174", "j"], ["8033020", "d.log"], ["5626152", "d.ext"], ["7214296", "k"],
      ...>                                   ]) |> Advent07.best_choice_for_deletion(70_000_000, 30_000_000)
      24933642

  """
  @spec best_choice_for_deletion(NaryTree.t(), non_neg_integer(), non_neg_integer()) ::
          non_neg_integer()
  def best_choice_for_deletion(filesystem, total, minimum) do
    node = NaryTree.root(filesystem)
    unused = total - size(node, filesystem)
    needed = minimum - unused

    do_get_directories([node], filesystem, [])
    |> Enum.map(fn n -> size(n, filesystem) end)
    |> Enum.filter(fn s -> s > needed end)
    |> Enum.sort()
    |> hd()
  end
end
