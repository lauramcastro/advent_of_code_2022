defmodule Advent01 do
  @moduledoc """
  Documentation for `Advent01`.
  """

  @doc """
  Find the maximum amount in a list of consecutive, grouped values.

  ## Examples

      iex> Advent01.maximum_calories([1000,2000,3000,nil,4000,nil,5000,6000,nil,7000,8000,9000,nil,10000])
      24000

      iex> Advent01.maximum_calories([1000,2000,3000,nil,4000,nil,5000,6000,nil,7000,8000,9000,10000])
      34000

  """
  @spec maximum_calories(list(integer())) :: integer()
  def maximum_calories(list) do
    do_maximum_calories(list, 0, 0)
  end

  defp do_maximum_calories([], acc, max) when acc > max, do: acc
  defp do_maximum_calories([], _acc, max), do: max

  defp do_maximum_calories([nil | rest], acc, max) when acc > max,
    do: do_maximum_calories(rest, 0, acc)

  defp do_maximum_calories([nil | rest], _, max), do: do_maximum_calories(rest, 0, max)

  defp do_maximum_calories([value | rest], acc, max),
    do: do_maximum_calories(rest, value + acc, max)

  @doc """
  Find the N maximum amount in a list of consecutive, grouped values.

  Of course, maximum_calories/1 could now be just top_maximum_calories/2 with N=1,
  but the original implementation is more efficient for that particular case, since
  it does not need to handle, nor sort, a potentially long list.

  ## Examples

      iex> Advent01.top_maximum_calories(3, [1000,2000,3000,nil,4000,nil,5000,6000,nil,7000,8000,9000,nil,10000])
      45000

      iex> Advent01.top_maximum_calories(3, [1000,2000,3000,nil,4000,nil,5000,6000,nil,7000,8000,9000,10000])
      51000

  """
  @spec top_maximum_calories(non_neg_integer(), list(integer())) :: integer()
  def top_maximum_calories(n, list) do
    do_sum_groups_of_calories(list, 0, [])
    |> Enum.sort(:desc)
    |> Enum.take(n)
    |> Enum.sum()
  end

  defp do_sum_groups_of_calories([], sum, acc), do: [sum | acc]

  defp do_sum_groups_of_calories([nil | rest], sum, acc),
    do: do_sum_groups_of_calories(rest, 0, [sum | acc])

  defp do_sum_groups_of_calories([value | rest], sum, acc),
    do: do_sum_groups_of_calories(rest, value + sum, acc)
end
