defmodule Advent01Test do
  use ExUnit.Case
  doctest Advent01

  test "answer to first challenge" do
    measurements =
      Utils.get_input("priv/input", fn
        "" -> nil
        x -> String.to_integer(x)
      end)

    assert Advent01.maximum_calories(measurements) == 72_017
  end

  test "answer to second challenge" do
    measurements =
      Utils.get_input("priv/input", fn
        "" -> nil
        x -> String.to_integer(x)
      end)

    assert Advent01.top_maximum_calories(3, measurements) == 212_520
  end
end
