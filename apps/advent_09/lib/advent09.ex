defmodule Advent09 do
  @moduledoc """
  Documentation for `Advent09`.
  """

  @doc """
  Calculates number of visited positions by the tail of a rope,
  after a sequence of moves.

  ## Examples

      iex> Advent09.visited([{"R", 4}, {"U", 4}, {"L", 3}, {"D", 1}, {"R", 4}, {"D", 1}, {"L", 5}, {"R", 2}])
      13

  """
  @spec visited(list({String.t(), non_neg_integer()})) :: non_neg_integer()
  def visited(moves) do
    do_visited(moves, {0, 0}, {0, 0}, [{0, 0}])
    |> Enum.uniq()
    |> Enum.count()
  end

  defp do_visited([], _h, t, visited), do: [t | visited]

  defp do_visited([{"R", amount} | more_moves], {rh, ch}, {rt, ct}, visited) do
    # h vai ir de {rh, ch} até {rh, ch+amount}
    [{rh, ch} | moves] = ch..(ch + amount) |> Enum.map(fn x -> {rh, x} end)
    {tail_pos, tail_trail} = chase({rt, ct}, moves, [])
    do_visited(more_moves, {rh, ch + amount}, tail_pos, tail_trail ++ visited)
  end

  defp do_visited([{"L", amount} | more_moves], {rh, ch}, {rt, ct}, visited) do
    # h vair ir de {rh, ch} até {rh-amount, ch}
    [{rh, ch} | moves] = ch..(ch - amount) |> Enum.map(fn x -> {rh, x} end)
    {tail_pos, tail_trail} = chase({rt, ct}, moves, [])
    do_visited(more_moves, {rh, ch - amount}, tail_pos, tail_trail ++ visited)
  end

  defp do_visited([{"U", amount} | more_moves], {rh, ch}, {rt, ct}, visited) do
    # h vair ir de {rh, ch} até {rh+amount, ch}
    [{rh, ch} | moves] = rh..(rh + amount) |> Enum.map(fn x -> {x, ch} end)
    {tail_pos, tail_trail} = chase({rt, ct}, moves, [])
    do_visited(more_moves, {rh + amount, ch}, tail_pos, tail_trail ++ visited)
  end

  defp do_visited([{"D", amount} | more_moves], {rh, ch}, {rt, ct}, visited) do
    # h vair ir de {rh, ch} até {rh-amount, ch}
    [{rh, ch} | moves] = rh..(rh - amount) |> Enum.map(fn x -> {x, ch} end)
    {tail_pos, tail_trail} = chase({rt, ct}, moves, [])
    do_visited(more_moves, {rh - amount, ch}, tail_pos, tail_trail ++ visited)
  end

  # warning ahead: this is probably the ugliest code I have ever written

  defp chase(pos, [], acc), do: {pos, acc}

  defp chase({r, c}, [{r, c} | more_moves], acc),
    do: chase({r, c}, more_moves, acc)

  defp chase({r, ct}, [{r, c} | more_moves], acc) when abs(c - ct) == 1,
    do: chase({r, ct}, more_moves, acc)

  defp chase({r, ct}, [{r, c} | more_moves], acc) when c - ct == 2,
    do: chase({r, ct + 1}, more_moves, [{r, ct} | acc])

  defp chase({r, ct}, [{r, c} | more_moves], acc) when ct - c == 2,
    do: chase({r, ct - 1}, more_moves, [{r, ct} | acc])

  defp chase({rt, c}, [{r, c} | more_moves], acc) when abs(rt - r) == 1,
    do: chase({rt, c}, more_moves, acc)

  defp chase({rt, c}, [{r, c} | more_moves], acc) when r - rt == 2,
    do: chase({rt + 1, c}, more_moves, [{rt, c} | acc])

  defp chase({rt, c}, [{r, c} | more_moves], acc) when rt - r == 2,
    do: chase({rt - 1, c}, more_moves, [{rt, c} | acc])

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when abs(r - rt) == 1 and abs(c - ct) == 1,
    do: chase({rt, ct}, more_moves, acc)

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when r - rt == 2 and c - ct == 1,
    do: chase({rt + 1, ct + 1}, more_moves, [{rt, ct} | acc])

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when r - rt == 1 and c - ct == 2,
    do: chase({rt + 1, ct + 1}, more_moves, [{rt, ct} | acc])

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when r - rt == 1 and ct - c == 2,
    do: chase({rt + 1, ct - 1}, more_moves, [{rt, ct} | acc])

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when rt - r == 1 and c - ct == 2,
    do: chase({rt - 1, ct + 1}, more_moves, [{rt, ct} | acc])

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when rt - r == 2 and c - ct == 1,
    do: chase({rt - 1, ct + 1}, more_moves, [{rt, ct} | acc])

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when rt - r == 2 and ct - c == 1,
    do: chase({rt - 1, ct - 1}, more_moves, [{rt, ct} | acc])

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when rt - r == 1 and ct - c == 2,
    do: chase({rt - 1, ct - 1}, more_moves, [{rt, ct} | acc])

  defp chase({rt, ct}, [{r, c} | more_moves], acc) when r - rt == 2 and ct - c == 1,
    do: chase({rt + 1, ct - 1}, more_moves, [{rt, ct} | acc])
end
