defmodule Advent09Test do
  use ExUnit.Case
  doctest Advent09

  test "answer to first challenge" do
    moves =
      Utils.get_input("priv/input", fn x ->
        [direction, length] = String.split(x)
        {direction, String.to_integer(length)}
      end)

    assert Advent09.visited(moves) == 6_384
  end
end
