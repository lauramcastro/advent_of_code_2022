defmodule Advent06 do
  @moduledoc """
  Documentation for `Advent06`.
  """

  @doc """
  Returns the end position of the start-of-packet
  (i.e. the first sequence of 4 different characters).

  ## Examples

      iex> "mjqjpqmgbljsphdztnvjfqwrcgsmlb" |> String.graphemes() |> Advent06.start_of_packet()
      7

      iex> "bvwbjplbgvbhsrlpgdmjqwftvncz" |> String.graphemes() |> Advent06.start_of_packet()
      5

      iex> "nppdvjthqldpwncqszvftbrmjlhg" |> String.graphemes() |> Advent06.start_of_packet()
      6

      iex> "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg" |> String.graphemes() |> Advent06.start_of_packet()
      10

      iex> "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw" |> String.graphemes() |> Advent06.start_of_packet()
      11

  """
  @spec start_of_packet(list(String.t())) :: non_neg_integer()
  def start_of_packet(datastream, size \\ 4) do
    do_start_of_packet(datastream, size, size)
  end

  # no base case because we always find something!
  defp do_start_of_packet(datastream = [_a | rest], size, pos) do
    if Enum.take(datastream, size) |> all_different?(),
      do: pos,
      else: do_start_of_packet(rest, size, pos + 1)
  end

  defp all_different?(list), do: length(Enum.uniq(list)) == length(list)

  @doc """
  Returns the end position of the start-of-message
  (i.e. the first sequence of 14 different characters).

  ## Examples

      iex> "mjqjpqmgbljsphdztnvjfqwrcgsmlb" |> String.graphemes() |> Advent06.start_of_message()
      19

      iex> "bvwbjplbgvbhsrlpgdmjqwftvncz" |> String.graphemes() |> Advent06.start_of_message()
      23

      iex> "nppdvjthqldpwncqszvftbrmjlhg" |> String.graphemes() |> Advent06.start_of_message()
      23

      iex> "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg" |> String.graphemes() |> Advent06.start_of_message()
      29

      iex> "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw" |> String.graphemes() |> Advent06.start_of_message()
      26

  """
  @spec start_of_message(list(String.t())) :: non_neg_integer()
  def start_of_message(datastream) do
    start_of_packet(datastream, 14)
  end
end
