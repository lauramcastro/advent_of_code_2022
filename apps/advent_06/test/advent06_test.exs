defmodule Advent06Test do
  use ExUnit.Case
  doctest Advent06

  test "answer to first challenge" do
    [datastream] = Utils.get_input("priv/input", fn x -> String.graphemes(x) end)

    assert Advent06.start_of_packet(datastream) == 1093
  end

  test "answer to second challenge" do
    [datastream] = Utils.get_input("priv/input", fn x -> String.graphemes(x) end)

    assert Advent06.start_of_message(datastream) == 3534
  end
end
