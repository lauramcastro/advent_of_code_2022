defmodule Advent11Test do
  use ExUnit.Case
  doctest Advent11

  test "answer to sample" do
    monkeys = %{
      0 => %{
        :items => [79, 98],
        :operation => fn x -> x * 19 end,
        :divisible => 23,
        :next_monkey => {2, 3},
        :inspected => 0
      },
      1 => %{
        :items => [54, 65, 75, 74],
        :operation => fn x -> x + 6 end,
        :divisible => 19,
        :next_monkey => {2, 0},
        :inspected => 0
      },
      2 => %{
        :items => [79, 60, 97],
        :operation => fn x -> x * x end,
        :divisible => 13,
        :next_monkey => {1, 3},
        :inspected => 0
      },
      3 => %{
        :items => [74],
        :operation => fn x -> x + 3 end,
        :divisible => 17,
        :next_monkey => {0, 1},
        :inspected => 0
      }
    }

    assert Advent11.monkey_business(monkeys, 3) == 10_605
  end

  # pending: read monkey specs from priv/input
  test "answer to first challenge" do
    monkeys = %{
      0 => %{
        :items => [62, 92, 50, 63, 62, 93, 73, 50],
        :operation => fn x -> x * 7 end,
        :divisible => 2,
        :next_monkey => {7, 1},
        :inspected => 0
      },
      1 => %{
        :items => [51, 97, 74, 84, 99],
        :operation => fn x -> x + 3 end,
        :divisible => 7,
        :next_monkey => {2, 4},
        :inspected => 0
      },
      2 => %{
        :items => [98, 86, 62, 76, 51, 81, 95],
        :operation => fn x -> x + 4 end,
        :divisible => 13,
        :next_monkey => {5, 4},
        :inspected => 0
      },
      3 => %{
        :items => [53, 95, 50, 85, 83, 72],
        :operation => fn x -> x + 5 end,
        :divisible => 19,
        :next_monkey => {6, 0},
        :inspected => 0
      },
      4 => %{
        :items => [59, 60, 63, 71],
        :operation => fn x -> x * 5 end,
        :divisible => 11,
        :next_monkey => {5, 3},
        :inspected => 0
      },
      5 => %{
        :items => [92, 65],
        :operation => fn x -> x * x end,
        :divisible => 5,
        :next_monkey => {6, 3},
        :inspected => 0
      },
      6 => %{
        :items => [78],
        :operation => fn x -> x + 8 end,
        :divisible => 3,
        :next_monkey => {0, 7},
        :inspected => 0
      },
      7 => %{
        :items => [84, 93, 54],
        :operation => fn x -> x + 1 end,
        :divisible => 17,
        :next_monkey => {2, 1},
        :inspected => 0
      }
    }

    assert Advent11.monkey_business(monkeys, 3) == 90_882
  end

  # test "answer to sample (second part)" do
  #   monkeys = %{
  #     0 => %{
  #       :items => [79, 98],
  #       :operation => fn x -> x * 19 end,
  #       :divisible => 23,
  #       :next_monkey => {2, 3},
  #       :inspected => 0
  #     },
  #     1 => %{
  #       :items => [54, 65, 75, 74],
  #       :operation => fn x -> x + 6 end,
  #       :divisible => 19,
  #       :next_monkey => {2, 0},
  #       :inspected => 0
  #     },
  #     2 => %{
  #       :items => [79, 60, 97],
  #       :operation => fn x -> x * x end,
  #       :divisible => 13,
  #       :next_monkey => {1, 3},
  #       :inspected => 0
  #     },
  #     3 => %{
  #       :items => [74],
  #       :operation => fn x -> x + 3 end,
  #       :divisible => 17,
  #       :next_monkey => {0, 1},
  #       :inspected => 0
  #     }
  #   }

  #   assert Advent11.monkey_business(monkeys, 96_577, 10_000) == 2_713_310_158
  # end

  # test "answer to second challenge" do
  #   monkeys = %{
  #     0 => %{
  #       :items => [62, 92, 50, 63, 62, 93, 73, 50],
  #       :operation => fn x -> x * 7 end,
  #       :divisible => 2,
  #       :next_monkey => {7, 1},
  #       :inspected => 0
  #     },
  #     1 => %{
  #       :items => [51, 97, 74, 84, 99],
  #       :operation => fn x -> x + 3 end,
  #       :divisible => 7,
  #       :next_monkey => {2, 4},
  #       :inspected => 0
  #     },
  #     2 => %{
  #       :items => [98, 86, 62, 76, 51, 81, 95],
  #       :operation => fn x -> x + 4 end,
  #       :divisible => 13,
  #       :next_monkey => {5, 4},
  #       :inspected => 0
  #     },
  #     3 => %{
  #       :items => [53, 95, 50, 85, 83, 72],
  #       :operation => fn x -> x + 5 end,
  #       :divisible => 19,
  #       :next_monkey => {6, 0},
  #       :inspected => 0
  #     },
  #     4 => %{
  #       :items => [59, 60, 63, 71],
  #       :operation => fn x -> x * 5 end,
  #       :divisible => 11,
  #       :next_monkey => {5, 3},
  #       :inspected => 0
  #     },
  #     5 => %{
  #       :items => [92, 65],
  #       :operation => fn x -> x * x end,
  #       :divisible => 5,
  #       :next_monkey => {6, 3},
  #       :inspected => 0
  #     },
  #     6 => %{
  #       :items => [78],
  #       :operation => fn x -> x + 8 end,
  #       :divisible => 3,
  #       :next_monkey => {0, 7},
  #       :inspected => 0
  #     },
  #     7 => %{
  #       :items => [84, 93, 54],
  #       :operation => fn x -> x + 1 end,
  #       :divisible => 17,
  #       :next_monkey => {2, 1},
  #       :inspected => 0
  #     }
  #   }

  #   assert Advent11.monkey_business(monkeys, 1, 10000) == 90_882
  # end
end
