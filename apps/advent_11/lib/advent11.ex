defmodule Advent11 do
  @moduledoc """
  Documentation for `Advent11`.
  """

  @type monkey :: %{
          items: list(integer()),
          operation: fun(),
          divisible: integer(),
          next_monkey: {non_neg_integer(), non_neg_integer()},
          inspected: non_neg_integer()
        }

  @doc """
  Calculates the operation of some funny monkeys.

  """
  @spec monkey_business(Map.t(), non_neg_integer(), non_neg_integer()) :: integer()
  def monkey_business(monkeys, stress_reductor, times \\ 20) do
    [first, second] =
      do_monkey_business(monkeys, stress_reductor, times)
      |> Enum.map(fn {_id, m} -> m.inspected end)
      |> Enum.sort(:desc)
      |> Enum.take(2)

    first * second
  end

  defp do_monkey_business(monkeys, _sr, 0), do: monkeys

  defp do_monkey_business(monkeys, sr, n) do
    Enum.reduce(0..(Enum.count(monkeys) - 1), monkeys, fn m, acc -> do_monkey_turn(m, sr, acc) end)
    |> do_monkey_business(sr, n - 1)
  end

  defp do_monkey_turn(n, sr, monkeys) do
    m = monkeys[n]
    inspected = length(m.items)

    Enum.reduce(m.items, monkeys, fn worry_level, acc ->
      {t, f} = m.next_monkey
      new_worry = m.operation.(worry_level) |> div(sr)

      {nnext, mnext} =
        if rem(new_worry, m.divisible) == 0,
          do: {t, acc[t]},
          else: {f, acc[f]}

      Map.replace!(acc, nnext, %{mnext | :items => mnext.items ++ [new_worry]})
    end)
    |> Map.replace!(n, %{m | :items => [], :inspected => m.inspected + inspected})
  end
end
