defmodule Advent04 do
  @moduledoc """
  Documentation for `Advent04`.
  """

  @doc """
  Counts the number of overlapping pairs in a list.

  ## Examples

      iex> Advent04.overlapping([[2,4,6,8],[2,3,4,5],[5,7,7,9],[2,8,3,7],[6,6,4,6],[2,6,4,8]])
      2

  """
  @spec overlapping(list(list(integer()))) :: integer()
  def overlapping(pairs) do
    do_overlapping(pairs, 0)
  end

  defp do_overlapping([], acc), do: acc

  defp do_overlapping([[a1, b1, a2, b2] | rest], acc) when a1 <= a2 and b1 >= b2,
    do: do_overlapping(rest, acc + 1)

  defp do_overlapping([[a1, b1, a2, b2] | rest], acc) when a2 <= a1 and b2 >= b1,
    do: do_overlapping(rest, acc + 1)

  defp do_overlapping([[_a1, _b1, _a2, _b2] | rest], acc), do: do_overlapping(rest, acc)

  @doc """
  Counts the number of intersecting pairs in a list.

  ## Examples

      iex> Advent04.intersecting([[2,4,6,8],[2,3,4,5],[5,7,7,9],[2,8,3,7],[6,6,4,6],[2,6,4,8]])
      4

  """
  @spec intersecting(list(list(integer()))) :: integer()
  def intersecting(pairs) do
    do_intersecting(pairs, 0)
  end

  defp do_intersecting([], acc), do: acc

  defp do_intersecting([[a1, b1, a2, b2] | rest], acc),
    do:
      if(Range.disjoint?(Range.new(a1, b1), Range.new(a2, b2)),
        do: do_intersecting(rest, acc),
        else: do_intersecting(rest, acc + 1)
      )
end
