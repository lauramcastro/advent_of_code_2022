defmodule Advent04Test do
  use ExUnit.Case
  doctest Advent04

  test "answer to first challenge" do
    pairs =
      Utils.get_input("priv/input", fn x ->
        String.split(x, [",", "-"]) |> Enum.map(&String.to_integer/1)
      end)

    assert Advent04.overlapping(pairs) == 466
  end

  test "answer to second challenge" do
    pairs =
      Utils.get_input("priv/input", fn x ->
        String.split(x, [",", "-"]) |> Enum.map(&String.to_integer/1)
      end)

    assert Advent04.intersecting(pairs) == 865
  end
end
