defmodule Advent02 do
  @moduledoc """
  Documentation for `Advent02`.
  """

  @rock_p1     "A"
  @paper_p1    "B"
  @scissors_p1 "C"
  @rock_p2     "X"
  @paper_p2    "Y"
  @scissors_p2 "Z"
  @score %{@rock_p2 => 1, @paper_p2 => 2, @scissors_p2 => 3}
  @win  6
  @lose 0
  @draw 3
  @should_lose "X"
  @should_draw "Y"
  @should_win  "Z"
  @loser  %{@rock_p1 => @scissors_p2, @paper_p1 => @rock_p2, @scissors_p1 => @paper_p2}
  @winner %{@rock_p1 => @paper_p2, @paper_p1 => @scissors_p2, @scissors_p1 => @rock_p2}
  @drawer %{@rock_p1 => @rock_p2, @paper_p1 => @paper_p2, @scissors_p1 => @scissors_p2}

  @doc """
  Calculates the score of a series of Rock-Paper-Scissors plays.

  ## Examples

      iex> Advent02.score([["A", "Y"],["B", "X"],["C", "Z"]])
      15

  """
  @spec score(list(list(String.t()))) :: integer()
  def score(plays) do
    do_score(plays, 0)
  end

  defp do_score([], score), do: score
  defp do_score([[@rock_p1,     p2 = @rock_p2]     | rest], score), do: do_score(rest, @score[p2] + @draw + score)
  defp do_score([[@rock_p1,     p2 = @paper_p2]    | rest], score), do: do_score(rest, @score[p2] + @win  + score)
  defp do_score([[@rock_p1,     p2 = @scissors_p2] | rest], score), do: do_score(rest, @score[p2] + @lose + score)
  defp do_score([[@paper_p1,    p2 = @rock_p2]     | rest], score), do: do_score(rest, @score[p2] + @lose + score)
  defp do_score([[@paper_p1,    p2 = @paper_p2]    | rest], score), do: do_score(rest, @score[p2] + @draw + score)
  defp do_score([[@paper_p1,    p2 = @scissors_p2] | rest], score), do: do_score(rest, @score[p2] + @win  + score)
  defp do_score([[@scissors_p1, p2 = @rock_p2]     | rest], score), do: do_score(rest, @score[p2] + @win  + score)
  defp do_score([[@scissors_p1, p2 = @paper_p2]    | rest], score), do: do_score(rest, @score[p2] + @lose + score)
  defp do_score([[@scissors_p1, p2 = @scissors_p2] | rest], score), do: do_score(rest, @score[p2] + @draw + score)

  @doc """
  Calculates the strategic score of a series of Rock-Paper-Scissors plays.

  ## Examples

      iex> Advent02.strategic_score([["A", "Y"]])
      4
      iex> Advent02.strategic_score([["B", "X"]])
      1
      iex> Advent02.strategic_score([["C", "Z"]])
      7
      iex> Advent02.strategic_score([["A", "Y"],["B", "X"],["C", "Z"]])
      12

  """
  @spec strategic_score(list(list(String.t()))) :: integer()
  def strategic_score(plays) do
    do_strategic_score(plays, 0)
  end

  defp do_strategic_score([], score), do: score
  defp do_strategic_score([[p1 = @rock_p1,     @should_win]  | rest], score), do: do_strategic_score(rest, @score[@winner[p1]] + @win  + score)
  defp do_strategic_score([[p1 = @rock_p1,     @should_lose] | rest], score), do: do_strategic_score(rest, @score[@loser[p1]]  + @lose + score)
  defp do_strategic_score([[p1 = @rock_p1,     @should_draw] | rest], score), do: do_strategic_score(rest, @score[@drawer[p1]] + @draw + score)
  defp do_strategic_score([[p1 = @paper_p1,    @should_win]  | rest], score), do: do_strategic_score(rest, @score[@winner[p1]] + @win  + score)
  defp do_strategic_score([[p1 = @paper_p1,    @should_lose] | rest], score), do: do_strategic_score(rest, @score[@loser[p1]]  + @lose + score)
  defp do_strategic_score([[p1 = @paper_p1,    @should_draw] | rest], score), do: do_strategic_score(rest, @score[@drawer[p1]] + @draw + score)
  defp do_strategic_score([[p1 = @scissors_p1, @should_win]  | rest], score), do: do_strategic_score(rest, @score[@winner[p1]] + @win  + score)
  defp do_strategic_score([[p1 = @scissors_p1, @should_lose] | rest], score), do: do_strategic_score(rest, @score[@loser[p1]]  + @lose + score)
  defp do_strategic_score([[p1 = @scissors_p1, @should_draw] | rest], score), do: do_strategic_score(rest, @score[@drawer[p1]] + @draw + score)

end
