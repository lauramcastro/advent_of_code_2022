defmodule Advent02Test do
  use ExUnit.Case
  doctest Advent02

  test "answer to first challenge" do
    plays = Utils.get_input("priv/input", fn x -> String.split(x) end)

    assert Advent02.score(plays) == 13_924
  end

  test "answer to second challenge" do
    plays = Utils.get_input("priv/input", fn x -> String.split(x) end)

    assert Advent02.strategic_score(plays) == 13_448
  end
end
