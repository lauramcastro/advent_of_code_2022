defmodule Advent08 do
  @moduledoc """
  Documentation for `Advent08`.
  """

  require Matrex

  @doc """
  Calculates the number of visible trees from outside a grid.

  ## Examples

      iex> Advent08.visible_trees([[3,0,3,7,3],[2,5,5,1,2],[6,5,3,3,2],[3,3,5,4,9],[3,5,3,9,0]])
      21

  """
  @spec visible_trees(list(list(String.t()))) :: non_neg_integer()
  def visible_trees(forest_description) do
    forest = Matrex.new(forest_description)
    {rows, columns} = Matrex.size(forest)
    # edges are always visible
    visibles = 2 * rows + 2 * columns - 4
    rows_to_inspect = 2..(rows - 1)
    columns_to_inspect = 2..(columns - 1)

    visibles +
      (Enum.flat_map(
         rows_to_inspect,
         fn r -> Enum.map(columns_to_inspect, fn c -> {r, c} end) end
       )
       |> Enum.filter(fn {row, column} -> visible?(forest, row, column, {rows, columns}) end)
       |> Enum.count())
  end

  defp visible?(forest, row, column, {total_rows, total_columns}) do
    element = Matrex.at(forest, row, column)
    horizontal_set = Matrex.row(forest, row)
    vertical_set = Matrex.column(forest, column)
    left_set = Enum.slice(horizontal_set, 0..(column - 2))
    right_set = Enum.slice(horizontal_set, column..total_columns)
    upward_set = Enum.slice(vertical_set, 0..(row - 2))
    downward_set = Enum.slice(vertical_set, row..total_rows)

    greater_than?(element, left_set) or greater_than?(element, right_set) or
      greater_than?(element, upward_set) or greater_than?(element, downward_set)
  end

  defp greater_than?(element, list), do: Enum.all?(list, fn x -> x < element end)

  @doc """
  Calculates the highest scenic score, that is, number of visible trees from a given tree.

  There's a lot of duplicated code with the previous half of this module,
  some refactoring would be advisable!

  ## Examples

      iex> Advent08.highest_scenic_score([[3,0,3,7,3],[2,5,5,1,2],[6,5,3,3,2],[3,3,5,4,9],[3,5,3,9,0]])
      8

  """
  @spec highest_scenic_score(list(list(String.t()))) :: non_neg_integer()
  def highest_scenic_score(forest_description) do
    forest = Matrex.new(forest_description)
    {rows, columns} = Matrex.size(forest)
    rows_to_inspect = 2..(rows - 1)
    columns_to_inspect = 2..(columns - 1)

    Enum.flat_map(
      rows_to_inspect,
      fn r -> Enum.map(columns_to_inspect, fn c -> {r, c} end) end
    )
    |> Enum.map(fn {row, column} -> scenic_score(forest, row, column, {rows, columns}) end)
    |> Enum.max()
  end

  defp scenic_score(forest, row, column, {total_rows, total_columns}) do
    element = Matrex.at(forest, row, column)
    horizontal_set = Matrex.row(forest, row)
    vertical_set = Matrex.column(forest, column)
    left_set = Enum.slice(horizontal_set, 0..(column - 2))
    right_set = Enum.slice(horizontal_set, column..total_columns)
    upward_set = Enum.slice(vertical_set, 0..(row - 2))
    downward_set = Enum.slice(vertical_set, row..total_rows)

    sequence(element, Enum.reverse(left_set)) * sequence(element, right_set) *
      sequence(element, Enum.reverse(upward_set)) * sequence(element, downward_set)
  end

  defp sequence(element, list) do
    inferior_to_element = Enum.take_while(list, fn x -> x < element end) |> Enum.count()
    edge = Enum.at(list, inferior_to_element)

    if edge != nil and edge >= element, do: inferior_to_element + 1, else: inferior_to_element
  end
end
