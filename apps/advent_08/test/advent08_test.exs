defmodule Advent08Test do
  use ExUnit.Case
  doctest Advent08

  test "answer to first challenge" do
    forest =
      Utils.get_input("priv/input", fn x ->
        String.graphemes(x) |> Enum.map(fn s -> String.to_integer(s) end)
      end)

    assert Advent08.visible_trees(forest) == 1_560
  end

  test "answer to second challenge" do
    forest =
      Utils.get_input("priv/input", fn x ->
        String.graphemes(x) |> Enum.map(fn s -> String.to_integer(s) end)
      end)

    assert Advent08.highest_scenic_score(forest) == 252_000
  end
end
