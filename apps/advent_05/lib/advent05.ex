defmodule Advent05 do
  @moduledoc """
  Documentation for `Advent05`.
  """

  @doc """
  Executes a number of element moves on a set of lists.

  ## Examples

      iex> Advent05.perform([{1, 2, 1}], [["N", "Z"],["D", "C", "M"],["P"]])
      "DCP"

      iex> Advent05.perform([{1, 2, 1}, {3, 1, 3}], [["N", "Z"],["D", "C", "M"],["P"]])
      "CZ"

      iex> Advent05.perform([{1, 2, 1}, {3, 1, 3}, {2, 2, 1}], [["N", "Z"],["D", "C", "M"],["P"]])
      "MZ"

      iex> Advent05.perform([{1, 2, 1}, {3, 1, 3}, {2, 2, 1}, {1, 1, 2}], [["N", "Z"],["D", "C", "M"],["P"]])
      "CMZ"

  """
  @spec perform(
          list({non_neg_integer(), non_neg_integer(), non_neg_integer()}),
          list(list(String.t())),
          :one_by_one | :bulk
        ) :: String.t()
  def perform(moves, columns, opt \\ :one_by_one)

  def perform(moves, columns, opt) do
    do_perform(moves, columns, opt)
  end

  defp do_perform([], columns, _opt) do
    columns
    |> Enum.reduce([], fn x, acc -> [head(x) | acc] end)
    |> Enum.reverse()
    |> Enum.join()
  end

  defp do_perform([{amount, orig, dest} | rest], columns, opt) do
    orig_column = Enum.at(columns, orig - 1)
    dest_column = Enum.at(columns, dest - 1)
    {to_move, to_remain} = Enum.split(orig_column, amount)

    List.replace_at(columns, orig - 1, to_remain)
    |> List.replace_at(dest - 1, move(opt, to_move) ++ dest_column)
    |> then(fn x -> do_perform(rest, x, opt) end)
  end

  defp head([]), do: ""
  defp head(x), do: hd(x)

  defp move(:one_by_one, list), do: Enum.reverse(list)
  defp move(:bulk, list), do: list
end
