defmodule Advent05Test do
  use ExUnit.Case
  doctest Advent05

  # pending: read columns from priv/input_columns"
  @columns [
    ["G", "P", "N", "R"],
    ["H", "V", "S", "C", "L", "B", "J", "T"],
    ["L", "N", "M", "B", "D", "T"],
    ["B", "S", "P", "V", "R"],
    ["H", "V", "M", "W", "S", "Q", "C", "G"],
    ["J", "B", "D", "C", "S", "Q", "W"],
    ["L", "Q", "F"],
    ["V", "F", "L", "D", "T", "H", "M", "W"],
    ["F", "J", "M", "V", "B", "P", "L"]
  ]

  test "answer to first challenge" do
    moves =
      Utils.get_input("priv/input_moves", fn x ->
        [amount, orig, dest] =
          Regex.run(~r/move (\d+\d*) from (\d) to (\d)/, x, capture: :all_but_first)

        {String.to_integer(amount), String.to_integer(orig), String.to_integer(dest)}
      end)

    assert Advent05.perform(moves, @columns) == "HBTMTBSDC"
  end

  test "answer to second challenge" do
    moves =
      Utils.get_input("priv/input_moves", fn x ->
        [amount, orig, dest] =
          Regex.run(~r/move (\d+\d*) from (\d) to (\d)/, x, capture: :all_but_first)

        {String.to_integer(amount), String.to_integer(orig), String.to_integer(dest)}
      end)

    assert Advent05.perform(moves, @columns, :bulk) == "PQTJRSHWS"
  end
end
