defmodule Advent03Test do
  use ExUnit.Case
  doctest Advent03

  test "answer to first challenge" do
    rucksacks =
      Utils.get_input("priv/input", fn x ->
        x |> String.split_at(div(length(String.graphemes(x)), 2))
      end)

    assert Advent03.weighted_sum(rucksacks) == 7_848
  end

  test "answer to second challenge" do
    rucksacks = Utils.get_input("priv/input", fn x -> x end)

    assert Advent03.weighted_triples_sum(rucksacks) == 2_616
  end
end
