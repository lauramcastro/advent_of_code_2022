defmodule Advent03 do
  @moduledoc """
  Documentation for `Advent03`.
  """

  @doc """
  Calculates a weighted sum for duplicated elements in a tuple.

  ## Examples

      iex> Advent03.weighted_sum([{"vJrwpWtwJgWr", "hcsFMMfFFhFp"}])
      16

      iex> Advent03.weighted_sum([{"jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL"}])
      38

      iex> Advent03.weighted_sum([{"PmmdzqPrV", "vPwwTWBwg"}])
      42

      iex> Advent03.weighted_sum([{"wMqvLMZHhHMvwLH", "jbvcjnnSBnvTQFn"}])
      22

      iex> Advent03.weighted_sum([{"ttgJtRGJ", "QctTZtZT"}])
      20

      iex> Advent03.weighted_sum([{"CrZsJsPPZsGz", "wwsLwLmpwMDw"}])
      19

      iex> Advent03.weighted_sum([{"vJrwpWtwJgWr", "hcsFMMfFFhFp"}, {"jqHRNqRjqzjGDLGL", "rsFMfFZSrLrFZsSL"}, {"PmmdzqPrV", "vPwwTWBwg"}, {"wMqvLMZHhHMvwLH", "jbvcjnnSBnvTQFn"}, {"ttgJtRGJ", "QctTZtZT"}, {"CrZsJsPPZsGz", "wwsLwLmpwMDw"}])
      157

  """
  @spec weighted_sum(list({String.t(), String.t()})) :: integer()
  def weighted_sum(rucksack) do
    do_weighted_sum(rucksack, 0)
  end

  defp do_weighted_sum([], sum), do: sum

  defp do_weighted_sum([{first, second} | rest], sum) do
    element = intersection([first, second])
    do_weighted_sum(rest, weight(element) + sum)
  end

  defp intersection(list) do
    for l <- list do
      l |> String.graphemes() |> MapSet.new()
    end
    |> Enum.reduce(fn s, acc -> MapSet.intersection(s, acc) end)
    |> MapSet.to_list()
    |> hd()
    |> String.to_charlist()
    |> hd()
  end

  defp weight(element) when element >= ?a and element <= ?z, do: element - 96
  defp weight(element), do: element - 64 + 26

  @doc """
  Calculates a weighted sum for duplicated elements in a set of N tuples.

  ## Examples

      iex> Advent03.weighted_triples_sum(["vJrwpWtwJgWrhcsFMMfFFhFp", "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", "PmmdzqPrVvPwwTWBwg"])
      18

      iex> Advent03.weighted_triples_sum(["wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", "ttgJtRGJQctTZtZT", "CrZsJsPPZsGzwwsLwLmpwMDw"])
      52

      iex> Advent03.weighted_triples_sum(["vJrwpWtwJgWrhcsFMMfFFhFp", "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL", "PmmdzqPrVvPwwTWBwg", "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn", "ttgJtRGJQctTZtZT", "CrZsJsPPZsGzwwsLwLmpwMDw"])
      70

  """
  @spec weighted_triples_sum(list(String.t())) :: integer()
  def weighted_triples_sum(rucksack) do
    do_weighted_triples_sum(rucksack, 0)
  end

  defp do_weighted_triples_sum([], sum), do: sum

  defp do_weighted_triples_sum([first, second, third | rest], sum) do
    element = intersection([first, second, third])
    do_weighted_triples_sum(rest, weight(element) + sum)
  end
end
